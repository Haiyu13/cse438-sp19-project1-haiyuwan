package com.example.myapplication

import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.activity_main.*
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.add_item.view.*
import kotlinx.android.synthetic.main.dialog.view.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        reset.setOnClickListener({

            goal.setTextColor(android.graphics.Color.BLACK)
            goal.setText("0");
            textView4.setText("0");



        //creat the arraylist of food and cals
        var food = ArrayList<String>();
        var cals = ArrayList<Int>();


        //build alertDialog

            val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog, null)
            val mBuilder = AlertDialog.Builder(this).setView(mDialogView).setTitle("Enter Total Calories")
            val mAlertDialog = mBuilder.show()
            mDialogView.submit.setOnClickListener({
                mAlertDialog.dismiss()
                val cals = mDialogView.yourgoal.text.toString()
                if (cals.equals("")) {
                    goal.setText("0")
                } else {
                    goal.setText(cals)
                }
            })


            var listView = findViewById<ListView>(R.id.customlistview)
            listView.adapter = MyCustomAdapter(this, food, cals)


        // add item button
        button2.setOnClickListener {

            val mDialogView2 = LayoutInflater.from(this).inflate(R.layout.add_item, null)
            val mBuilder2 = AlertDialog.Builder(this)
                .setView(mDialogView2)
                .setTitle("Enter items")
            val mAlertDialog2 = mBuilder2.show()
            mDialogView2.addbt.setOnClickListener({
                mAlertDialog2.dismiss()
                food.add(mDialogView2.foodadd.text.toString())
                if (mDialogView2.calsadd.text.toString() == "") {
                    cals.add(0)
                } else {
                    cals.add(mDialogView2.calsadd.text.toString().toInt())
                }

                listView.adapter = MyCustomAdapter(this, food, cals)
                var total_cals: Int = 0
                var remain_cals: Int = goal.text.toString().toInt()
                for (i in cals) {
                    total_cals = total_cals + i

                }
                remain_cals = remain_cals - cals[cals.size - 1]
                if (remain_cals < 0) {
                    goal.setTextColor(android.graphics.Color.RED)
                } else {
                    goal.setTextColor(android.graphics.Color.BLACK)
                }
                textView4.setText(total_cals.toString())
                goal.setText(remain_cals.toString())
            })

            // calculate part

        }
        })
    }


    //adapter
    private class MyCustomAdapter(context: Context, food: ArrayList<String>, cals: ArrayList<Int>): BaseAdapter(){
        private val mContext: Context
        private var foodList = ArrayList<String>()
        private var calsList = ArrayList<Int>()
        init {
            mContext = context
            foodList=food
            calsList=cals
        }
        override fun getCount(): Int {
            return foodList.size;
        }
        override fun getItemId(position: Int): Long {
            return position.toLong()
        }
        override fun getItem(position: Int): Any {
            return "test string"
        }
        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val rowmain = layoutInflater.inflate(R.layout.format,viewGroup , false)

            val foodname =rowmain.findViewById<TextView>(R.id.food)
            val calsname = rowmain.findViewById<TextView>(R.id.cals)
            foodname.text = foodList.get(position)
            calsname.text = ""+calsList.get(position)
            return rowmain
        }
    }
}
