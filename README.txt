creation part:


I have add a button called start;

it will not show the dialog until you click

it will also let you try to reset the cals and the menu again.

1. (10 / 10 Points) User can enter total calorie amount on start up
2. (8 / 8 Points) User can add new food item by name
3. (8 / 8 Points) User can add new food item by calorie
4. (4 / 4 Points) Adding new food items is done in a second activity
	-4 Only one activity
5. (5 / 5 Points) Calories remaining is updated with each new food item
6. (5 / 5 Points) Calorie consumed is updated with each new food item
7. (10 / 10 Points) The list of food items displays foods and their respective calories amounts
8. (10 / 10 Points) Color change when calorie count becomes negative
9. (10 / 10 Points) All inputs are filtered and error messages are displayed accordingly
	-3 Leaving the fields blank inserts zeroes onto the list
10. (2 / 2 Points) Code is clean and commented
11. (3 / 3 Points) App is visually appealing
12. (5 / 15 Points) Creative portion - design your own feature(s)!
	-10 Adding a button is not sufficient for the creative portion

Total: 73 / 90
